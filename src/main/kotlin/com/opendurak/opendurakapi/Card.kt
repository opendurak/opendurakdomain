package com.opendurak.opendurakapi

data class Card(
    val suit: Suit,
    val value: Value,
) {
    enum class Suit { HEART, DIAMOND, CLUB, SPADE }
    enum class Value(val weight: Int) {
        SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10),
        JACK(11), QUEEN(12), KING(13), ACE(14),
    }
}