package com.opendurak.opendurakapi

data class PlayerWithOpenCards(
    val hash: String,
    val cards: List<Card>,
)