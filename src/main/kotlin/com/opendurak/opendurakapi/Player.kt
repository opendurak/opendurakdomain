package com.opendurak.opendurakapi

data class Player(
    val hash: String,
    val username: String,
    val cardCount: Int,
)